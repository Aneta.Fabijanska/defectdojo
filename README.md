# GitLab CI template for DefectDojo

This project implements a GitLab CI/CD template for [DefectDojo](https://github.com/DefectDojo/django-DefectDojo) dashboard.

DefectDojo is a security program and vulnerability management tool that allows you to consolidate all the vulnerabilities of your project into one source of truth.

:warning: By default, DefectDojo will collect vulnerabilities only on your `master` or your `main` branch. For collecting vulnerabilities on another branch, set `PROD_REF` variable to your branch name.

Please read [DefectDojo documentation](https://defectdojo.github.io/django-DefectDojo/) for further information.

## Get started

### DefectDojo instance

You must have access to a DefectDojo server and set `DEFECTDOJO_SERVER_URL` variable according to this server's URL.

### Define variables in your project

The name of the DefectDojo product will be set to the `CI_PROJECT_PATH` variable of the GitLab project.

The initial mandatory step for a developer is to retrieve his DefectDojo API v2 key.
Once logged into DefectDojo, retrieve your API v2 key by clicking on the user logo in the top-right corner.
Store this API key into a variable named `DEFECTDOJO_API_KEY`.

### Check you use at least one security tool

DefectDojo job will NOT launch security tools: it will only import security reports that have been generated on YOUR demand in your `.gitlab-ci.yml`. We strongly advise you to use [to be continuous templates](https://gitlab.com/to-be-continuous) to launch security tools.
Some security tools are already included in more generic templates (such as [python](https://gitlab.com/to-be-continuous/python)), but other security tools are the object of a specific template (such as [sonar](https://gitlab.com/to-be-continuous/sonar)).

So make sure that your `.gitlab-ci.yml` config launches at least one of the [supported security tools](#supported-security-tools), otherwise the current template will be useless.

### Every day usage in your pipelines

Every time your CI/CD pipeline is launched, if you have at least one security tool which generates a security report, then all of the newly generated security reports will be imported into DefectDojo, you have nothing to do.

You may connect to DefectDojo server to visualize the summary of your project's vulnerabilities. You might also receive an e-mail notification if your project contains new vulnerabilities, depending on the threshold you defined in `DEFECTDOJO_NOTIFICATION_SEVERITIES` and on the fact you defined `DEFECTDOJO_SMTP_SERVER`.

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component)
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: $CI_SERVER_FQDN/to-be-continuous/defectdojo/gitlab-ci-defectdojo@2.6.5
    # 2: set/override component inputs
    inputs:
      server-url: "https://defectdojo.acme.host" # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: "to-be-continuous/defectdojo"
    ref: "2.6.5"
    file: "/templates/gitlab-ci-defectdojo.yml"

variables:
  # 2: set/override template variables
  DEFECTDOJO_SERVER_URL: "https://defectdojo.acme.host" # ⚠ this is only an example
```

## defectdojo job

This job uploads security reports found in the artifacts to a DefectDojo server.

It is bound to the `.post` stage, and uses the following variables:

| Input / Variable                       | Description                                                                                  | Default value |
| -------------------------------------- | -------------------------------------------------------------------------------------------- | ------------- |
| `dir` / `DEFECTDOJO_DIR`               | DefectDojo working directory                                                                 | `.`           |
| `server-url` / `DEFECTDOJO_SERVER_URL` | URL of DefectDojo server                                                                     | _none_        |
| :lock: `DEFECTDOJO_API_KEY`            | Your DefectDojo API v2 Key                                                                   | _none_        |

## Global variables

Depending on the security tools you launch, and their configuration, you may have to redefine some of the following variables in your `gitlab-ci.yml`:

| Input / Variable                                                         | Description                                                                                                                            | Default value                                                                              |
| ------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ |
| `bandit-reports` / `DEFECTDOJO_BANDIT_REPORTS`                           | path to Bandit JSON reports                                                                                                            | `bandit*.json reports/py-bandit.bandit.json`                                               |
| `dc-gradle-reports` / `DEFECTDOJO_DC_GRADLE_REPORTS`                     | path to Dependency Check XML reports (Gradle template)                                                                                 | `dependency-check*.xml`                                                                    |
| `dc-reports` / `DEFECTDOJO_DC_REPORTS`                                   | path to Dependency Check XML reports (Maven template)                                                                                  | `dependency-check*.xml`                                                                    |
| `gitleaks-reports` / `DEFECTDOJO_GITLEAKS_REPORTS`                       | path to Gitleaks JSON reports                                                                                                          | `gitleaks/gitleaks-report.json reports/gitleaks.native.json`                               |
| `hadolint-reports` / `DEFECTDOJO_HADOLINT_REPORTS`                       | path to Hadolint JSON reports                                                                                                          | `hadolint-json-*.json reports/docker-hadolint-*.native.json`                               |
| `mobsf-reports` / `DEFECTDOJO_MOBSF_REPORTS`                             | path to MobSF JSON reports                                                                                                             | `mobsf*.json`                                                                              |
| `nodejsscan-reports` / `DEFECTDOJO_NODEJSSCAN_REPORTS`                   | path to NodeJSScan SARIF reports                                                                                                       | `nodejsscan-report-sarif.json`                                                             |
| `npmaudit-reports` / `DEFECTDOJO_NPMAUDIT_REPORTS`                       | path to NPM Audit JSON reports                                                                                                         | `npm-audit*.json`                                                                          |
| `semgrep-reports` / `DEFECTDOJO_SEMGREP_REPORTS`                         | Path to Semgrep reports                                                                                                                | `reports/semgrep.native.json`                                                              |
| `semgrep-template` / `SEMGREP_TEMPLATE`                                  | Path to Semgrep template                                                                                                               | _none_                                                                                     |
| `testssl-reports` / `DEFECTDOJO_TESTSSL_REPORTS`                         | path to TestSSL CSV reports                                                                                                            | `reports/testssl.native.csv`                                                               |
| `zap-reports` / `DEFECTDOJO_ZAP_REPORTS`                                 | path to Zap xml reports                                                                                                                | `reports/zap.native.xml`                                                                   |
| `zap-tpl-project` / `ZAP_TPL_PROJECT`                                    | path to Zap template (if any)                                                                                                          | _none_                                                                                     |
| `trivy-reports` / `DEFECTDOJO_TRIVY_REPORTS`                             | path to Trivy JSON reports                                                                                                             | `trivy/*.json trivy-*.json reports/docker-trivy-*.native.json reports/py-trivy.trivy.json` |
| `DEFECTDOJO_SONARQUBE_APPLICATION`                                       | your application name in Sonar                                                                                                         | ex: project-name                                                                           |
| `DEFECTDOJO_SONARQUBE_PROJECT_KEY`                                       | your project key in Sonar (be careful, the project key might be distinct from project-dir-project-name, sometimes Sonar adds a suffix) | ex: project-dir-project-name or project-dir-project-name\_\_AXjLJ76cpGZrj3BUYg10           |
| `sonarqube-sinceleakperiod` / `DEFECTDOJO_SONARQUBE_SINCELEAKPERIOD`     | determines if delta analysis is activated                                                                                              | false                                                                                      |
| `sonarqube-nosecurityhotspot` / `DEFECTDOJO_SONARQUBE_NOSECURITYHOTSPOT` | if true, disable hotspots processing                                                                                                   | true                                                                                       |
| `DEFECTDOJO_SONARQUBE_ALLBUGS `                                          | true if all bugs are returned, false if only vulnerabilities are returned                                                              | false                                                                                      |
| `base-image` / `DEFECTDOJO_BASE_IMAGE`                                   | Alpine base image                                                                                                                      | `registry.hub.docker.com/library/node:alpine3.11`                                          |
| `smtp-server` / `DEFECTDOJO_SMTP_SERVER`                                 | name:port of SMTP server used for notifications                                                                                        | _none_ (email notifications disabled)                                                      |
| `notification-severities` / `DEFECTDOJO_NOTIFICATION_SEVERITIES`         | list of (comma separated) severities to be notified on                                                                                 | Critical,High                                                                              |
| `timezone` / `DEFECTDOJO_TIMEZONE`                                       | timezone used for naming engagements and setting engagements start and end                                                             | `Europe/Paris`                                                                             |
| `noprod-enabled` / `DEFECTDOJO_NOPROD_ENABLED`                           | determines if defectdojo job is launched on non production branches                                                                    | false                                                                                      |

## Supported security tools

For now, the current template allows to import reports from the following tools:

- Bandit
- Dependency Check
- Gitleaks
- Hadolint
- MobSF
- NodeJSScan
- NPM Audit
- Semgrep
- SonarQube
- TestSSL
- Trivy
- Zap

## SonarQube specific case

SonarQube is specific: contrary to other jobs, sonar job does not generate any artifact, but just launches an analysis on SonarQube server. In `defectdojo` job, a tool named `sonar-report` generates an HTML report thanks to Sonar API ; this report is finally imported in DefectDojo.

In order to launch SonarQube HTML-report generation, the following variables must be set:

- SONAR_HOST_URL (used by sonar template)
- SONAR_TOKEN (used by sonar template)
- DEFECTDOJO_SONARQUBE_APPLICATION
- DEFECTDOJO_SONARQUBE_PROJECT_KEY

The following variables are set by default, but may be overriden:

- DEFECTDOJO_SONARQUBE_SINCELEAKPERIOD (default: false)
- DEFECTDOJO_SONARQUBE_NOSECURITYHOTSPOT (default: true)
- DEFECTDOJO_SONARQUBE_ALLBUGS (default: false)

The following command is executed:

```yaml
sonar-report \
--sonarurl="${SONAR_HOST_URL}" \
--sonarcomponent="${DEFECTDOJO_SONARQUBE_PROJECT_KEY}" \
--project="${DEFECTDOJO_SONARQUBE_PROJECT_KEY}" \
--application="${DEFECTDOJO_SONARQUBE_APPLICATION}" \
--branch="${CI_COMMIT_REF_NAME}" \
--sonartoken="${SONAR_TOKEN}" \
--sinceleakperiod="${DEFECTDOJO_SONARQUBE_SINCELEAKPERIOD}" \
--noSecurityHotspot="${DEFECTDOJO_SONARQUBE_NOSECURITYHOTSPOT}" \
--allbugs="${DEFECTDOJO_SONARQUBE_ALLBUGS}" > "$sonar_report"
```

## Manual actions during pipeline

Since some security tools (Bandit, Safety...) must be launched manually in the pipeline, proceed in two steps:

- click manually on the corresponding jobs
- when they are all finished, click on defectdojo-publish job to launch it manually

## Version history

See [Tags page](https://gitlab.com/to-be-continuous/defectdojo/-/tags)

## Variants

The DefectDojo template can be used in conjunction with template variants to cover specific cases.

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Input / Variable                    | Description                                                                                                                     | Default value                                                              |
| ----------------------------------- | ------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| `TBC_VAULT_IMAGE`                   | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest` |
| `vault-base-url` / `VAULT_BASE_URL` | The Vault server base API url                                                                                                   | **must be defined**                                                        |
| `vault-oidc-aud` / `VAULT_OIDC_AUD` | The `aud` claim for the JWT                                                                                                     | `$CI_SERVER_URL`                                                           |
| :lock: `VAULT_ROLE_ID`              | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID                                                             | _none_                                                                     |
| :lock: `VAULT_SECRET_ID`            | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID                                                           | _none_                                                                     |

By default, the variant will authentifacte using a [JWT ID token](https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html). To use [AppRole](https://www.vaultproject.io/docs/auth/approle) instead the `VAULT_ROLE_ID` and `VAULT_SECRET_ID` should be defined as secret project variables.

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Parameter                        | Description                                                           |
| -------------------------------- | --------------------------------------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server                      |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - component: $CI_SERVER_FQDN/to-be-continuous/defectdojo/gitlab-ci-defectdojo@2.6.5
    inputs:
      server-url: "https://defectdojo.acme.host" # ⚠ this is only an example
      # Secrets managed by Vault
      api-key: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/defectdojo/credentials?field=token"
  # Vault variant
  - component: $CI_SERVER_FQDN/to-be-continuous/defectdojo/gitlab-ci-defectdojo-vault@2.6.5
    inputs:
      # audience claim for JWT
      vault-oidc-aud: "https://vault.acme.host"
      vault-base-url: "https://vault.acme.host/v1"
```
